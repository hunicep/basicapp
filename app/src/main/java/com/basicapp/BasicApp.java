package com.basicapp;

import android.app.Application;
import com.basicapp.dagger.AppModule;
import com.basicapp.dagger.DaggerDataComponent;
import com.basicapp.dagger.DataComponent;
import com.basicapp.dagger.DataModule;

public class BasicApp extends Application {
  private DataComponent mDataComponent;

  @Override public void onCreate() {
    super.onCreate();

    mDataComponent = DaggerDataComponent.builder()
        .appModule(new AppModule(this))
        .dataModule(new DataModule())
        .build();
  }

  public DataComponent getDataComponent() {
    return mDataComponent;
  }
}
