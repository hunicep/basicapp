package com.basicapp.moviedetails;

public interface MovieDetailsContract {
  interface View {
    void showMovieTitle(String title);

    void showMoviePoster(String posterUrl);

    void showMovieOverview(String overview);

    void showMovieReleaseDate(String releaseDate);

    void showMovieVoteAverage(String voteAverage);

    void showMovieVoteCount(String voteCount);

    void showMoviePosterPlaceHolder();
  }

  interface UserActionsListener {
    void loadMovie(Long movieId);
  }
}
