package com.basicapp.moviedetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.basicapp.R;
import com.basicapp.util.Constants;
import com.squareup.picasso.Picasso;

public class MovieDetailsFragment extends Fragment implements MovieDetailsContract.View {
  @Bind(R.id.text_movie_title) TextView movieTitle;
  @Bind(R.id.image_movie_poster) ImageView moviePoster;
  @Bind(R.id.text_movie_overview) TextView movieOverview;
  @Bind(R.id.text_movie_release_date) TextView movieReleaseDate;
  @Bind(R.id.text_movie_vote_average) TextView movieVoteAverage;
  @Bind(R.id.text_movie_vote_count) TextView movieVoteCount;
  private MovieDetailsContract.UserActionsListener mUserActionsListener;

  public MovieDetailsFragment() {
  }

  public static MovieDetailsFragment newInstance(Long movieId) {
    Bundle args = new Bundle();
    args.putLong(Constants.MOVIE_ID_EXTRA_NAME, movieId);
    MovieDetailsFragment fragment = new MovieDetailsFragment();
    fragment.setArguments(args);
    return fragment;
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_movie_details, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    mUserActionsListener = new MovieDetailsPresenter(this, getActivity().getApplication());
  }

  @Override public void onResume() {
    super.onResume();

    if (getArguments() != null) {
      mUserActionsListener.loadMovie(getArguments().getLong(Constants.MOVIE_ID_EXTRA_NAME, -1));
    }
  }

  @Override public void showMovieTitle(String title) {
    movieTitle.setText(title);
  }

  @Override public void showMoviePoster(String posterUrl) {
    Picasso.with(getActivity())
        .load(String.format(getActivity().getResources().getString(R.string.url_movie_poster),
            posterUrl))
        .placeholder(R.drawable.ic_detail_poster_placeholder)
        .into(moviePoster);
  }

  @Override public void showMovieOverview(String overview) {
    movieOverview.setText(overview);
  }

  @Override public void showMovieReleaseDate(String releaseDate) {
    movieReleaseDate.setText(releaseDate);
  }

  @Override public void showMovieVoteAverage(String voteAverage) {
    movieVoteAverage.setText(voteAverage);
  }

  @Override public void showMovieVoteCount(String voteCount) {
    movieVoteCount.setText(voteCount);
  }

  @Override public void showMoviePosterPlaceHolder() {
    moviePoster.setImageDrawable(
        ResourcesCompat.getDrawable(getResources(), R.drawable.ic_detail_poster_placeholder, null));
  }
}
