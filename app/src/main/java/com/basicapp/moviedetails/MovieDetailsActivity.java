package com.basicapp.moviedetails;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.basicapp.R;
import com.basicapp.util.Constants;

public class MovieDetailsActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_movie_details);

    if (savedInstanceState == null) {
      MovieDetailsFragment movieDetailsFragment = MovieDetailsFragment.newInstance(
          getIntent().getLongExtra(Constants.MOVIE_ID_EXTRA_NAME, -1));
      FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      transaction.add(R.id.fragment_movie_details_placeholder, movieDetailsFragment);
      transaction.commit();
    }
  }
}
