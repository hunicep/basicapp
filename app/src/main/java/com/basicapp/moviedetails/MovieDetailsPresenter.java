package com.basicapp.moviedetails;

import android.app.Application;
import android.text.TextUtils;
import com.basicapp.data.Movie;
import com.basicapp.data.MovieDataContract;
import com.basicapp.data.MovieLoader;

public class MovieDetailsPresenter
    implements MovieDetailsContract.UserActionsListener, MovieDataContract.DataReceiver {
  private MovieDetailsContract.View mMovieDetailsView;
  private MovieDataContract.DataLoader mMovieDataLoader;

  public MovieDetailsPresenter(MovieDetailsContract.View movieDetailsView,
      Application application) {
    mMovieDetailsView = movieDetailsView;
    mMovieDataLoader = new MovieLoader(this, application);
  }

  @Override public void loadMovie(Long movieId) {
    if (movieId != -1) {
      mMovieDataLoader.getMovie(movieId);
    }
  }

  @Override public void onMovieLoaded(Movie movie) {
    if (!TextUtils.isEmpty(movie.getTitle())) {
      mMovieDetailsView.showMovieTitle(movie.getTitle());
    } else {
      mMovieDetailsView.showMovieTitle("Not available");
    }

    if (!TextUtils.isEmpty(movie.getPosterPath()) && movie.getPosterPath() != null) {
      mMovieDetailsView.showMoviePoster(movie.getPosterPath());
    } else {
      mMovieDetailsView.showMoviePosterPlaceHolder();
    }

    if (!TextUtils.isEmpty(movie.getOverview())) {
      mMovieDetailsView.showMovieOverview(movie.getOverview());
    } else {
      mMovieDetailsView.showMovieOverview("Not available");
    }

    if (!TextUtils.isEmpty(movie.getReleaseDate())) {
      mMovieDetailsView.showMovieReleaseDate(movie.getReleaseDate());
    } else {
      mMovieDetailsView.showMovieReleaseDate("Not available");
    }

    if (movie.getVoteAverage() != null && movie.getVoteAverage() != 0.0) {
      mMovieDetailsView.showMovieVoteAverage(String.valueOf(Math.round(movie.getVoteAverage())));
    } else {
      mMovieDetailsView.showMovieVoteAverage("Not available");
    }

    if (movie.getVoteCount() != null && movie.getVoteCount() != 0) {
      mMovieDetailsView.showMovieVoteCount(String.valueOf(movie.getVoteCount()));
    } else {
      mMovieDetailsView.showMovieVoteCount("Not available");
    }
  }
}
