package com.basicapp.movies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.basicapp.R;
import com.basicapp.data.Movie;
import com.squareup.picasso.Picasso;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {
  private Context mContext;
  private List<Movie> mMovies;
  private MovieItemClickListener mListener;

  public MoviesAdapter(Context context, List<Movie> movieTitles, MovieItemClickListener listener) {
    mContext = context;
    mMovies = movieTitles;
    mListener = listener;
  }

  /**
   * Replaces the current Movies List
   *
   * @param upcomingMovies the new movies list
   */
  public void replaceUpcomingMoviesList(List<Movie> upcomingMovies) {
    mMovies.clear();
    mMovies.addAll(upcomingMovies);
    notifyDataSetChanged();
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View upcomingMovieView = inflater.inflate(R.layout.item_movies_list, parent, false);
    return new ViewHolder(upcomingMovieView);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    Movie movie = mMovies.get(position);

    holder.movieTitle.setText(movie.getTitle());
    Picasso.with(mContext)
        .load(String.format(mContext.getResources().getString(R.string.url_movie_poster),
            movie.getPosterPath()))
        .placeholder(R.drawable.ic_list_poster_placeholder)
        .fit()
        .centerInside()
        .into(holder.moviePoster);
  }

  @Override public int getItemCount() {
    return mMovies.size();
  }

  public interface MovieItemClickListener {
    void onMovieItemClick(Movie movie);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.image_movie_poster) ImageView moviePoster;
    @Bind(R.id.text_movie_title) TextView movieTitle;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View view) {
          if (mListener != null) {
            Movie movie = mMovies.get(getLayoutPosition());
            mListener.onMovieItemClick(movie);
          }
        }
      });
    }
  }
}
