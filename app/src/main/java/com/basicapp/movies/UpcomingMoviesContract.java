package com.basicapp.movies;

import com.basicapp.data.Movie;
import java.util.List;

public interface UpcomingMoviesContract {

  interface View {
    void showUpcomingMovies(List<Movie> upcomingMovies);

    void stopRefreshing();
  }

  interface UserActionsListener {
    void loadUpcomingMovies(boolean forceUpdate);

    void clearSubscriptions();
  }
}
