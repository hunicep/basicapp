package com.basicapp.movies;

import android.app.Application;
import android.util.Log;
import com.basicapp.data.Movie;
import com.basicapp.data.MoviesDataContract;
import com.basicapp.data.UpcomingMovies;
import com.basicapp.data.UpcomingMoviesLoader;
import java.util.List;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class UpcomingMoviesPresenter
    implements UpcomingMoviesContract.UserActionsListener, MoviesDataContract.DataReceiver {
  private UpcomingMoviesContract.View mUpcomingMoviesView;
  private MoviesDataContract.DataLoader mUpcomingMoviesDataLoader;
  private CompositeSubscription mCompositeSubscription;

  public UpcomingMoviesPresenter(UpcomingMoviesContract.View upcomingMoviesView,
      Application application) {
    mUpcomingMoviesView = upcomingMoviesView;
    mUpcomingMoviesDataLoader = new UpcomingMoviesLoader(this, application);
    mCompositeSubscription = new CompositeSubscription();
  }

  @Override public void loadUpcomingMovies(boolean forceUpdate) {
    mUpcomingMoviesDataLoader.getUpcomingMovies(forceUpdate);
  }

  @Override public void clearSubscriptions() {
    mCompositeSubscription.clear();
  }

  @Override public void onUpcomingMoviesLoaded(Observable<List<Movie>> upcomingMovies) {
    mCompositeSubscription.add(upcomingMovies.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Action1<List<Movie>>() {
          @Override public void call(List<Movie> movies) {
            mUpcomingMoviesView.stopRefreshing();
            mUpcomingMoviesView.showUpcomingMovies(movies);
          }
        }, new Action1<Throwable>() {
          @Override public void call(Throwable throwable) {
            Log.d(UpcomingMovies.class.getSimpleName(), throwable.getMessage());
          }
        }));
  }
}
