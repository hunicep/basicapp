package com.basicapp.movies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.basicapp.R;
import com.basicapp.data.Movie;
import com.basicapp.moviedetails.MovieDetailsActivity;
import com.basicapp.moviedetails.MovieDetailsFragment;
import com.basicapp.util.Constants;

public class MoviesActivity extends AppCompatActivity
    implements MoviesAdapter.MovieItemClickListener {
  private boolean mIsTwoPane = false;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_movies);

    if (findViewById(R.id.fragment_movie_details_placeholder) != null) {
      mIsTwoPane = true;
    }
  }

  @Override public void onMovieItemClick(Movie movie) {
    if (mIsTwoPane) {
      MovieDetailsFragment movieDetailsFragment = MovieDetailsFragment.newInstance(movie._id);
      FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      transaction.replace(R.id.fragment_movie_details_placeholder, movieDetailsFragment);
      transaction.commit();
    } else {
      Intent intent = new Intent(this, MovieDetailsActivity.class);
      intent.putExtra(Constants.MOVIE_ID_EXTRA_NAME, movie._id);
      startActivity(intent);
    }
  }
}
