package com.basicapp.movies;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.basicapp.R;
import com.basicapp.data.Movie;
import java.util.ArrayList;
import java.util.List;

public class UpcomingMoviesFragment extends Fragment
    implements UpcomingMoviesContract.View, SwipeRefreshLayout.OnRefreshListener {
  @Bind(R.id.list_upcoming_movies) RecyclerView upcomingMoviesList;
  @Bind(R.id.refresh_upcoming_movies) SwipeRefreshLayout upcomingMoviesRefresh;
  private MoviesAdapter mAdapter;
  private UpcomingMoviesContract.UserActionsListener mUserActionsListener;

  public UpcomingMoviesFragment() {
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mAdapter =
        new MoviesAdapter(getActivity(), new ArrayList<Movie>(), (MoviesActivity) getActivity());
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_upcoming_movies, container, false);

    ButterKnife.bind(this, view);

    upcomingMoviesList.setHasFixedSize(true);
    upcomingMoviesList.setAdapter(mAdapter);

    if (getActivity().getResources().getConfiguration().orientation
        == Configuration.ORIENTATION_PORTRAIT) {
      upcomingMoviesList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
    } else {
      upcomingMoviesList.setLayoutManager(new GridLayoutManager(getActivity(), 4));
    }

    upcomingMoviesRefresh.setOnRefreshListener(this);
    upcomingMoviesRefresh.setColorSchemeResources(R.color.colorAccent);

    return view;
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    mUserActionsListener = new UpcomingMoviesPresenter(this, getActivity().getApplication());
  }

  @Override public void onResume() {
    super.onResume();
    mUserActionsListener.loadUpcomingMovies(false);
  }

  @Override public void onStop() {
    super.onStop();
    mUserActionsListener.clearSubscriptions();
  }

  @Override public void showUpcomingMovies(List<Movie> upcomingMovies) {
    mAdapter.replaceUpcomingMoviesList(upcomingMovies);
  }

  @Override public void stopRefreshing() {
    if (upcomingMoviesRefresh.isRefreshing()) {
      upcomingMoviesRefresh.setRefreshing(false);
    }
  }

  @Override public void onRefresh() {
    mUserActionsListener.loadUpcomingMovies(true);
  }
}
