package com.basicapp.dagger;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import com.basicapp.data.MoviesDatabaseHelper;
import com.basicapp.data.TmdbService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dagger.Module;
import dagger.Provides;
import java.io.IOException;
import javax.inject.Singleton;
import nl.nl2312.rxcupboard.RxCupboard;
import nl.nl2312.rxcupboard.RxDatabase;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

@Module public class DataModule {
  private static final String API_BASE_URL = "http://api.themoviedb.org/3/";
  private static final String API_KEY_PARAM = "api_key";
  private static final String API_KEY = "5f882731f1b8bd4c769da1784ea175b9";

  @Provides @Singleton MoviesDatabaseHelper providesMoviesDatabaseHelper(Application application) {
    return new MoviesDatabaseHelper(application);
  }

  @Provides @Singleton SQLiteDatabase providesSqLiteDatabase(
      MoviesDatabaseHelper moviesDatabaseHelper) {
    return moviesDatabaseHelper.getWritableDatabase();
  }

  @Provides @Singleton RxDatabase providesRxDatabase(SQLiteDatabase sqLiteDatabase) {
    return RxCupboard.withDefault(sqLiteDatabase);
  }

  @Provides @Singleton OkHttpClient providesOkHttpClient() {
    return new OkHttpClient.Builder().addInterceptor(new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder().addQueryParameter(API_KEY_PARAM, API_KEY).build();
        request = request.newBuilder().url(url).build();
        return chain.proceed(request);
      }
    }).build();
  }

  @Provides @Singleton Gson providesGson() {
    return new GsonBuilder().create();
  }

  @Provides @Singleton RxJavaCallAdapterFactory providesCallAdapterFactory() {
    return RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
  }

  @Provides @Singleton Retrofit providesRetrofit(Gson gson, OkHttpClient client,
      RxJavaCallAdapterFactory callAdapterFactory) {
    return new Retrofit.Builder().baseUrl(API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(callAdapterFactory)
        .client(client)
        .build();
  }

  @Provides @Singleton TmdbService providesApiService(Retrofit retrofit) {
    return retrofit.create(TmdbService.class);
  }
}
