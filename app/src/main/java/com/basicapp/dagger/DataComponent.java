package com.basicapp.dagger;

import com.basicapp.data.MovieLoader;
import com.basicapp.data.UpcomingMoviesLoader;
import dagger.Component;
import javax.inject.Singleton;

@Singleton @Component(modules = { AppModule.class, DataModule.class })
public interface DataComponent {
  void inject(UpcomingMoviesLoader upcomingMoviesLoader);

  void inject(MovieLoader movieLoader);
}
