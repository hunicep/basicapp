package com.basicapp.dagger;

import android.app.Application;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module public class AppModule {
  private Application mApplication;

  public AppModule(Application application) {
    mApplication = application;
  }

  @Provides @Singleton Application providesApplication() {
    return mApplication;
  }
}
