package com.basicapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class MoviesDatabaseHelper extends SQLiteOpenHelper {
  private static final String DATABASE_NAME = "moviesDatabase.db";
  private static final int DATABASE_VERSION = 1;

  // Models Registration
  static {
    cupboard().register(Movie.class);
  }

  public MoviesDatabaseHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override public void onCreate(SQLiteDatabase db) {
    cupboard().withDatabase(db).createTables();
  }

  @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    cupboard().withDatabase(db).upgradeTables();
  }
}
