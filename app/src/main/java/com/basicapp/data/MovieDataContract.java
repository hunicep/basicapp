package com.basicapp.data;

public interface MovieDataContract {
  interface DataLoader {
    void getMovie(Long movieId);
  }

  interface DataReceiver {
    void onMovieLoaded(Movie movie);
  }
}
