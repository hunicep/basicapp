package com.basicapp.data;

import retrofit2.http.GET;
import rx.Observable;

public interface TmdbService {
  @GET("movie/upcoming") Observable<UpcomingMovies> listUpcomingMovies();
}
