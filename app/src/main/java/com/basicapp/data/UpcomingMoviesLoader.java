package com.basicapp.data;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import com.basicapp.BasicApp;
import java.util.List;
import javax.inject.Inject;
import nl.nl2312.rxcupboard.RxDatabase;
import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class UpcomingMoviesLoader implements MoviesDataContract.DataLoader {
  @Inject SQLiteDatabase database;
  @Inject RxDatabase rxDatabase;
  @Inject TmdbService service;

  private MoviesDataContract.DataReceiver mDataReceiver;

  public UpcomingMoviesLoader(MoviesDataContract.DataReceiver dataReceiver,
      Application application) {
    mDataReceiver = dataReceiver;
    ((BasicApp) application).getDataComponent().inject(this);
  }

  @Override public void getUpcomingMovies(boolean forceUpdate) {
    if (forceUpdate) {
      cupboard().withDatabase(database).delete(Movie.class, null);
    }

    Observable<List<Movie>> observable = Observable.defer(new Func0<Observable<List<Movie>>>() {
      @Override public Observable<List<Movie>> call() {
        List<Movie> upcomingMovies = cupboard().withDatabase(database).query(Movie.class).list();
        if (upcomingMovies != null && !upcomingMovies.isEmpty()) {
          return Observable.just(upcomingMovies);
        }

        Observable<UpcomingMovies> response = service.listUpcomingMovies();
        return response.flatMap(new Func1<UpcomingMovies, Observable<List<Movie>>>() {
          @Override public Observable<List<Movie>> call(UpcomingMovies response) {
            List<Movie> upcomingMovies = response.getMovies();
            cupboard().withDatabase(database).put(upcomingMovies);
            return Observable.just(upcomingMovies);
          }
        });
      }
    });

    mDataReceiver.onUpcomingMoviesLoaded(observable);
  }
}