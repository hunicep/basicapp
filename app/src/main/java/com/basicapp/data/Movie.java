package com.basicapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import nl.qbusict.cupboard.annotation.Column;

public class Movie {
  public Long _id;

  @SerializedName("id") @Column("movie_id") @Expose public Integer id;
  @SerializedName("poster_path") @Column("poster_path") @Expose public String posterPath;
  @SerializedName("overview") @Expose public String overview;
  @SerializedName("release_date") @Column("release_date") @Expose public String releaseDate;
  @SerializedName("title") @Expose public String title;
  @SerializedName("vote_average") @Column("vote_average") @Expose private Double voteAverage;
  @SerializedName("vote_count") @Column("vote_count") @Expose private Integer voteCount;

  /**
   * No args constructor for use in serialization
   */
  public Movie() {
  }

  /**
   * @param id
   * @param title
   * @param releaseDate
   * @param overview
   * @param posterPath
   * @param voteAverage
   */
  public Movie(String posterPath, String overview, String releaseDate, Integer id, String title,
      Double voteAverage, Integer voteCount) {
    this.posterPath = posterPath;
    this.overview = overview;
    this.releaseDate = releaseDate;
    this.id = id;
    this.title = title;
    this.voteAverage = voteAverage;
    this.voteCount = voteCount;
  }

  /**
   * @return the local database id
   */
  public Long getLocalId() {
    return _id;
  }

  /**
   * @return The posterPath
   */
  public String getPosterPath() {
    return posterPath;
  }

  /**
   * @param posterPath The poster_path
   */
  public void setPosterPath(String posterPath) {
    this.posterPath = posterPath;
  }

  /**
   * @return The overview
   */
  public String getOverview() {
    return overview;
  }

  /**
   * @param overview The overview
   */
  public void setOverview(String overview) {
    this.overview = overview;
  }

  /**
   * @return The releaseDate
   */
  public String getReleaseDate() {
    return releaseDate;
  }

  /**
   * @param releaseDate The release_date
   */
  public void setReleaseDate(String releaseDate) {
    this.releaseDate = releaseDate;
  }

  /**
   * @return The id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id The id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return The title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title The title
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return The voteAverage
   */
  public Double getVoteAverage() {
    return voteAverage;
  }

  /**
   * @param voteAverage The vote_average
   */
  public void setVoteAverage(Double voteAverage) {
    this.voteAverage = voteAverage;
  }

  /**
   * @return The voteCount
   */
  public Integer getVoteCount() {
    return voteCount;
  }

  /**
   * @param voteCount The vote_count
   */
  public void setVoteCount(Integer voteCount) {
    this.voteCount = voteCount;
  }
}
