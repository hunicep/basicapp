package com.basicapp.data;

import java.util.List;
import rx.Observable;

public interface MoviesDataContract {
  interface DataLoader {
    void getUpcomingMovies(boolean forceU);
  }

  interface DataReceiver {
    void onUpcomingMoviesLoaded(Observable<List<Movie>> upcomingMovies);
  }
}
