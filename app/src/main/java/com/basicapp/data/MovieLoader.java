package com.basicapp.data;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import com.basicapp.BasicApp;
import javax.inject.Inject;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class MovieLoader implements MovieDataContract.DataLoader {
  @Inject SQLiteDatabase database;

  private MovieDataContract.DataReceiver mDataReceiver;

  public MovieLoader(MovieDataContract.DataReceiver dataReceiver, Application application) {
    mDataReceiver = dataReceiver;
    ((BasicApp) application).getDataComponent().inject(this);
  }

  @Override public void getMovie(Long movieId) {
    Movie movie = cupboard().withDatabase(database).get(Movie.class, movieId);
    if (movie != null) {
      mDataReceiver.onMovieLoaded(movie);
    }
  }
}
