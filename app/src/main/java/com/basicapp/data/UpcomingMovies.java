package com.basicapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class UpcomingMovies {
  @SerializedName("results") @Expose private List<Movie> movies = new ArrayList<>();

  /**
   * No args constructor for use in serialization
   */
  public UpcomingMovies() {
  }

  /**
   * @param movies The upcoming movies
   */
  public UpcomingMovies(List<Movie> movies) {
    this.movies = movies;
  }

  /**
   * @return The upcoming movies
   */
  public List<Movie> getMovies() {
    return movies;
  }

  /**
   * @param movies The upcoming movies
   */
  public void setMovies(List<Movie> movies) {
    this.movies = movies;
  }
}
